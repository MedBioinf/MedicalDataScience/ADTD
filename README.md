# ADTD: Adaptive Digital Tissue Deconvolution

This repository implements ADTD as described in [1](ADDLINK) and provides an example application.
**ADTD** performs cell-type deconvolution to estimate cellular proportions in bulk data. Moreover, ADTD provides estimates of hidden background contributions and estimates of cell-type specific gene regulation.



### Requirements
**ADTD** requires the *quadprog* solver for *qpsolvers*, *numpy* and *scikit_learn*. You can install the requirements using *pip* and the requirements.txt file
```sh
pip install -r requirements.txt
```

### Example File
[ADTD_example.py](ADDLINK) shows an example application using artificially created bulk gene expression profiles `Y` with corresponding reference profiles `X` for five cell types together with gene weights `gamma`.
ADTD returns the estimated cellular proportions `C_hat`, the estimated cellular weights `c_hat` of the hidden background profile `x_hat` and the rescaling matrix `Delta`.


## References

[1] Franziska Görtler, et. al. "Adaptive Digital Tissue Deconvolution".
