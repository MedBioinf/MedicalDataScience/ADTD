# ADTD example script for the command line
import numpy as np
from ADTD import ADTD

if __name__ == "__main__":

    ### Load Data ###
    data_path = "Example_data/"
    X = np.load(data_path + "X_ref.npy").astype(np.double)          # Reference Profiles for 5 cell Types
    Y = np.load(data_path + "Y.npy").astype(np.double)              # 100 bulk gene expression profiles
    gamma = np.load(data_path + "gamma.npy").astype(np.double)      # gene weights gamma

    ### Run ADTD ###
    C_hat , c_hat, x_hat, Delta = ADTD(X, Y, gamma, lambda1=0.00001, lambda2=0.01, max_iterations=500, eps=1e-8)

    ### Save results ###
    np.save("C_hat.npy",C_hat)      # Estimated cellular proportions Matrix
    np.save("c_hat.npy",c_hat)      # Estimated cellular weights of the hidden background profile
    np.save("x_hat.npy",x_hat)      # Hidden background profile
    np.save("Delta.npy",Delta)      # rescaling matrix







