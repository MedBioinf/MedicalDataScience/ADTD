# ADTD source file
import numpy as np
from sklearn.linear_model import LinearRegression
from qpsolvers import solve_qp

def ADTD(X, Y, gamma, lambda1, lambda2=100.0, max_iterations=200, eps=1e-8, C_static=False, Delta_static=False, quiet=False):
    p, q = X.shape
    n = Y.shape[1]
    Y, X = Y/np.sum(Y, 0), X/np.sum(X, 0)
    G = np.diag(np.sqrt(gamma))
    Gamma = np.diag(gamma)
    J1n = np.ones((1,n))
    J1p = np.ones((1,p))
    J1q = np.ones((1,q))
    Delta = np.ones((p,q))
    GX, GY = G@X, G@Y

    reg_nnls = LinearRegression(fit_intercept=False,positive=True)
    C0_init = reg_nnls.fit(GX, GY).coef_.T
    x_init = np.mean(Y - X@C0_init, 1).reshape((p,1))
    x_init = np.maximum(x_init, 0)
    x = x_init/np.sum(x_init)
    C0 = np.zeros((q,n))
    sqrt_lam1 = 0

    def update_C(x, Delta):
        J1p = np.ones((1,p))
        J1n = np.ones((1,n))
        if C_static == True:
            c_new = (J1n - J1p@(Delta*X)@C0_init)
            return C0_init,c_new
        x = x/np.sum(x)
        Delta = Delta.copy()
        P_sqrt = np.r_[G@(Delta*X - x@J1p@(Delta*X)),
                       sqrt_lam1*np.identity(q)]
        P = 2.*P_sqrt.T@P_sqrt
        Qtemp = np.r_[GY - G@x@J1n,
                      sqrt_lam1*C0]
        Q = -2.*P_sqrt.T@Qtemp

        Gmat = np.r_[ - np.identity(q),
                     J1p@(Delta*X)]
        h = np.zeros(q+1);  h[q] = 1.
        C_new = np.zeros((q,n))
        for i in range(n):
            sol = solve_qp(P, Q[:,i], Gmat, h, solver='quadprog')
            C_new[:,i] = sol
        c_new = (J1n - J1p@(Delta*X)@C_new)
        return C_new, c_new

    C0,_ = update_C(x, Delta)
    sqrt_lam1 = np.sqrt(lambda1)

    def update_Delta(x0, C0, Delta0):
        if Delta_static==True:
            print("Delta_static=true")
            return np.ones((p,q))
        Delta = Delta0.copy()
        C = C0.copy()
        x = x0.copy()
        xbar = x/np.sum(x)
        for j in range(p):
            DeltaX_ne_j = (Delta * X)
            DeltaX_ne_j[j,:] = 0.
            G_ne_j = G.copy()
            G_ne_j[j,j] = 0.
            xbar_ne_j = xbar.copy()
            xbar_ne_j[j] = 0.
            CX_j = np.tile(X[j,:].reshape((q,1)), n)*C
            B = Y - xbar@J1n - (Delta * X)@C + xbar*J1p@DeltaX_ne_j@C
            B[j,:] = 0.
            A_j_row = Y[j,:] - xbar[j,0]*(J1n - J1p@DeltaX_ne_j@C)
            P = 2.*(G[j,j]**2*(1. - xbar[j,0])**2+np.sum(Gamma@(xbar_ne_j**2)))*CX_j@CX_j.T + 2*lambda2*np.identity(q)
            qT = -2.*(G[j,j]**2*(1. - xbar[j,0])*A_j_row-np.sum(Gamma@np.diag(xbar_ne_j.flatten())@B, axis=0))@CX_j.T - 2*lambda2*J1q
            Gmat = np.r_[- np.identity(q), CX_j.T]
            h = np.zeros(q + n)
            h[q:] = np.ones(n) - (J1p@DeltaX_ne_j@C).flatten()

            sol = solve_qp(P, qT[0,:], Gmat, h,solver="quadprog")

            Delta[j,:] = sol

        return Delta

    def update_x_new(c, Z):
        if np.all(c==0):
            return np.ones((p,1))/p
        P = 2.*Gamma*np.sum(c**2)
        q = - 2.*(c@Z.T@Gamma).flatten()
        G = - np.identity(p)
        h = np.zeros(p)
        A = np.ones(p)
        b = np.array([1.])
        sol = solve_qp(P, q, G, h, A, b ,solver="osqp")
        x = np.array(sol).reshape((p,1))
        x[x<0] = 0
        return x

    def Loss_ADTD(C, x, Delta):
        J1n = np.ones((1,n))
        J1p = np.ones((1,p))

        c = (J1n - J1p@((Delta*X)@C))/np.sum(x)
        ltemp = G@(Y - (Delta*X)@C - x@c)
        term1 = np.sum(ltemp**2)
        term2 = lambda1*np.sum((C-C0)**2)
        term3 = lambda2*np.sum((Delta-np.ones((p,q)))**2)
        return term1 + term2 + term3, term1, term2 + term3

    C = C0.copy()
    Xplus = np.c_[X, x]

    if quiet == False:
        print('start loss = %1.6e, start loss RSS = %1.6e, start loss bias = %1.6e' % Loss_ADTD(C, x, Delta)[0:])
    for i in range(max_iterations):
        C_old = C.copy()
        Delta_old = Delta.copy()
        x_old = x.copy()

        loss_old = Loss_ADTD(C_old, x_old, Delta_old)[0]

        if C_static==True:
            C = C0
            c = (J1n - J1p@(Delta*X)@C0)
        else:
            C, c = update_C(x, Delta)

        Z = Y - (Delta*X)@C

        x = update_x_new(c, Z)

        Delta = update_Delta(x, C, Delta)
        loss_new = Loss_ADTD(C, x, Delta)[0]
        err = loss_old - loss_new

        if i % 1 == 0:
            if quiet == False:
                ltemp = Loss_ADTD(C, x, Delta)
                print('lambda1 = %1.2e, lambda2 = %1.2e, i= %i, err = %1.2e, loss = %1.6e, loss RSS = %1.6e, loss bias = %1.6e' % (lambda1, lambda2,
                    i, err, ltemp[0],ltemp[1],ltemp[2]))

        if i > 1 and err < eps:
            break
    if quiet == False:
        ltemp = Loss_ADTD(C, x, Delta)
        print('lambda1 = %1.2e, lambda2 = %1.2e, i= %i, err = %1.2e, loss = %1.6e, loss RSS = %1.6e, loss bias = %1.6e' % (lambda1, lambda2,
            i, err, ltemp[0],ltemp[1],ltemp[2]))

    return C, c[0], x, Delta
